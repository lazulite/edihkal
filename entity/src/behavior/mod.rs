//! Contains custom logic for entities that are not generated by [`sea_orm`].
//!
//! This includes things like:
//! * `NewModel` types (e.g. [`NewDrug`][ND]) for [Models][M]
//! * [`ActiveModelBehavior`][AMB] implementation for [Models][M]
//! * Trait implementation for [Models][M]
//!
//! [AMB]: sea_orm::entity::ActiveModelBehavior
//! [M]: sea_orm::entity::ModelTrait
//! [ND]: crate::NewDrug
pub mod drug;
pub mod entry;
